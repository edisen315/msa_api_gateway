package com.example.apigatewayservice.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.HashMap;

@Component
@Slf4j
public class AuthorizationHeaderFilter extends AbstractGatewayFilterFactory<AuthorizationHeaderFilter.Config> {

    Environment environment;
    public AuthorizationHeaderFilter(Environment environment){
        super(Config.class);
        this.environment = environment;
    }

    public static class Config{

    }


    public static class NameConfig{

    }

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();

           if(!request.getHeaders().containsKey(HttpHeaders.AUTHORIZATION)){
               return onError(exchange, "No Authorization header", HttpStatus.UNAUTHORIZED);
           }

           String authrizationHeader = request.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
           String jwt = authrizationHeader.replace("Bearer", "");

           if(!isJwtValid(jwt)){
               return onError(exchange, "JWT token is not valid", HttpStatus.UNAUTHORIZED);
           }

           return chain.filter(exchange);

        });
    }

    private boolean isJwtValid(String jwt) {
        boolean returnValue = true;

        String subject = null;

        try {
            Claims claims = Jwts.parser().setSigningKey(environment.getProperty("token.secret"))
                    .parseClaimsJws(jwt)
                    .getBody();

            subject = claims.getSubject();

        } catch (ExpiredJwtException ex){
            returnValue = false; //토큰이 만료된 경우
        }
        catch (Exception ex){
            returnValue = false;
        }

        if(subject == null || subject.isEmpty()){
            returnValue = false;
        }

        // subject에는 userId가 들어있다. subJect와 요청한 email의 userId를 비교한 후 일치하면 로그인 진행하면 된다.

        return returnValue;
    }


    // 단일 : mono, 복수 : flux
    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response =  exchange.getResponse();
        response.setStatusCode(httpStatus);

        log.error(err);
        return response.setComplete();
    }
}
