package com.example.apigatewayservice.filter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class LoggingFliter extends AbstractGatewayFilterFactory<LoggingFliter.Config> {

    public LoggingFliter(){
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        // Custom Pre Filter
        return (exchange, chain) -> {
            ServerHttpRequest request =exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();

            log.info("LOGGING filter baseMessage: {}", config.getBaseMessage());

            if(config.isPostLogger()){
                log.info("LOGGING FILTER START : REQUEST ID -> {}", request.getId());
            }



        // Custom Post Filter
        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            if(config.isPostLogger()){
                log.info("LOGGING FILTER END : response code -> {}", response.getStatusCode());
            }
        }));


        };
    }

    @Data
    public static class Config{
        // Put the Configuration properties
        private String baseMessage;
        private boolean preLogger;
        private boolean postLogger;
    }

}
